package org.but.feec.eshop.data;

import org.but.feec.eshop.api.UserAuthView;
import org.but.feec.eshop.api.UsersView;
import org.but.feec.eshop.config.DataSourceConfig;
import org.but.feec.eshop.exceptions.DataAccessException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PersonRepository {
    public UserAuthView findUserByEmail(String email) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT email, password" +
                             " FROM eshop.users p" +
                             " WHERE p.email = ?")
        ) {
            preparedStatement.setString(1, email);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToUserAuthView(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("Find person by ID with addresses failed.", e);
        }
        return null;
    }

    private UserAuthView mapToUserAuthView(ResultSet rs) throws SQLException {
        UserAuthView user = new UserAuthView();
        user.setEmail(rs.getString("email"));
        user.setPassword(rs.getString("password"));
        return user;
    }

    private UsersView mapToUsersView(ResultSet rs) throws SQLException {
        UsersView personBasicView = new UsersView();
        personBasicView.setId(rs.getLong("iduser"));
        personBasicView.setEmail(rs.getString("email"));
        personBasicView.setFirstName(rs.getString("first_name"));
        personBasicView.setLastName(rs.getString("last_name"));
        personBasicView.setAge(rs.getInt("age"));
        return personBasicView;
    }
    public List<UsersView> getUsersBasicView() {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT iduser,first_name,last_name,age,email FROM eshop.users;");
             ResultSet resultSet = preparedStatement.executeQuery();) {
            List<UsersView> usersBasicView = new ArrayList<>();
            while (resultSet.next()) {
                usersBasicView.add(mapToUsersView(resultSet));
            }
            return usersBasicView;
        } catch (SQLException e) {
            throw new DataAccessException("Persons basic view could not be loaded.", e);
        }
    }
}

