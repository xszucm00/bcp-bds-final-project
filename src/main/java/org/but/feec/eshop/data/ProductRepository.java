package org.but.feec.eshop.data;

import org.but.feec.eshop.api.ProductDetailedView;
import org.but.feec.eshop.api.ProductEditView;
import org.but.feec.eshop.api.ProductInsertView;
import org.but.feec.eshop.api.ProductView;
import org.but.feec.eshop.config.DataSourceConfig;
import org.but.feec.eshop.exceptions.DataAccessException;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ProductRepository {
    public List<ProductView> getProductView() {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT p.idproduct,c.title AS category,brand,p.title,size,price,p.quantity,p.descrip,ready_to_sell,d.title AS discount" +
                             " FROM eshop.product p " +
                             " LEFT JOIN eshop.product_category r ON p.idproduct=r.idproduct" +
                             " LEFT JOIN eshop.category c ON c.idcategory=r.idcategory" +
                             " LEFT JOIN eshop.discount d ON p.iddiscount=d.iddiscount");
             ResultSet resultSet = preparedStatement.executeQuery();) {
            List<ProductView> productViews = new ArrayList<>();
            while (resultSet.next()) {
                productViews.add(mapToProductView(resultSet));
            }
            return productViews;
        } catch (SQLException e) {
            throw new DataAccessException("Product basic view could not be loaded.", e);
        }
    }

    public List<ProductView> getProductViewByBrand(String brand) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT p.idproduct,c.title AS category,brand,p.title,size,price,p.quantity,p.descrip,ready_to_sell,d.title AS discount" +
                             " FROM eshop.product p " +
                             " LEFT JOIN eshop.product_category r ON p.idproduct=r.idproduct" +
                             " LEFT JOIN eshop.category c ON c.idcategory=r.idcategory" +
                             " LEFT JOIN eshop.discount d ON p.iddiscount=d.iddiscount" +
                             " WHERE p.brand LIKE ?;")
        ) {
            preparedStatement.setString(1, brand + "%");
            List<ProductView> productViews = new ArrayList<>();
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    productViews.add(mapToProductView(resultSet));
                }
            }
            return productViews;
        } catch (SQLException e) {
            throw new DataAccessException("Find person by ID with addresses failed.", e);
        }
    }
    public ProductDetailedView getProductDetailedView(Long productId) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT p.idproduct,c.title AS category,brand,p.title,size,quantity,descrip,price,text AS review,rating,r.iduser AS from_user" +
                             " FROM eshop.product p" +
                             " LEFT JOIN eshop.product_review r" +
                             " ON p.idproduct=r.idproduct" +
                             " LEFT JOIN eshop.product_category o ON p.idproduct=o.idproduct" +
                             " LEFT JOIN eshop.category c ON c.idcategory=o.idcategory" +
                             " WHERE p.idproduct=?")
        ) {
            preparedStatement.setLong(1, productId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToDetailedProductView(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("Find person by ID with addresses failed.", e);
        }
        return null;
    }
    private ProductView mapToProductView(ResultSet rs) throws SQLException {
        ProductView productView = new ProductView();
        productView.setId(rs.getLong("idproduct"));
        productView.setCategory(rs.getString("category"));
        productView.setBrand(rs.getString("brand"));
        productView.setTitle(rs.getString("title"));
        productView.setSize(rs.getString("size"));
        productView.setPrice(rs.getDouble("price"));
        productView.setQuantity(rs.getString("quantity"));
        productView.setDesc(rs.getString("descrip"));
       // productView.setReady(rs.getBoolean("ready_to_sell"));
       // productView.setDiscount(rs.getString("discount"));
        return productView;
    }
    private ProductDetailedView mapToDetailedProductView(ResultSet rs) throws SQLException {
        ProductDetailedView productDetailedViewView = new ProductDetailedView();
        productDetailedViewView.setId(rs.getLong("idproduct"));
        productDetailedViewView.setCategory(rs.getString("category"));
        productDetailedViewView.setBrand(rs.getString("brand"));
        productDetailedViewView.setTitle(rs.getString("title"));
        productDetailedViewView.setSize(rs.getString("size"));
        productDetailedViewView.setPrice(rs.getDouble("price"));
        productDetailedViewView.setQuantity(rs.getString("quantity"));
        productDetailedViewView.setDesc(rs.getString("descrip"));
        productDetailedViewView.setIdUser(rs.getLong("from_user"));
        productDetailedViewView.setReview(rs.getString("review"));
        productDetailedViewView.setRating(rs.getString("rating"));
        return productDetailedViewView;
    }
    public void deleteProduct(Long id){
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "DELETE FROM eshop.product" +
                             " WHERE idproduct = ?;")
        ) {
            preparedStatement.setLong(1,id);
            preparedStatement.executeQuery();
        } catch (SQLException e) {
            throw new DataAccessException("Find person by ID with addresses failed.", e);
        }
    }
    public void editProduct(ProductEditView productEditView) {
        String UpdateProductSQL = "UPDATE eshop.product" +
                " SET title = ?,size = ?,price = ?,quantity=?,descrip=?,brand=?" +
                " WHERE idproduct=?;";

        try (Connection connection = DataSourceConfig.getConnection();

             PreparedStatement preparedStatement = connection.prepareStatement(UpdateProductSQL, Statement.RETURN_GENERATED_KEYS)) {

            preparedStatement.setString(1, productEditView.getTitle());
            preparedStatement.setString(2, productEditView.getSize());
            preparedStatement.setDouble(3, productEditView.getPrice());
            preparedStatement.setString(4, productEditView.getQuantity());
            preparedStatement.setString(5, productEditView.getDesc());
            preparedStatement.setString(6, productEditView.getBrand());
            preparedStatement.setLong(7, productEditView.getId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DataAccessException("Creating person failed operation on the database failed.");
        }
    }

    public void insertProduct(ProductInsertView productInsertView) {
        Connection connection = null;
        try {
            connection = DataSourceConfig.getConnection();
            connection.setAutoCommit(false);
            PreparedStatement findIdCategory = connection.prepareStatement(
                    "SELECT idcategory FROM eshop.category WHERE title LIKE ?;"
            );
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO eshop.product (title,size,price,quantity,descrip,brand)" +
                            "VALUES(?,?,?,?,?,?);" +
                            " " +
                            "INSERT INTO eshop.product_category (idproduct,idcategory)" +
                            "VALUES((SELECT MAX(idproduct) FROM eshop.product),?);"
            );
            findIdCategory.setString(1, productInsertView.getCategory() + "%");

            ResultSet categoryId = findIdCategory.executeQuery();
            Integer x =0;
            while(categoryId.next()){
                x=categoryId.getInt("idcategory");
            }

            preparedStatement.setString(1, productInsertView.getTitle());
            preparedStatement.setString(2, productInsertView.getSize());
            preparedStatement.setDouble(3, productInsertView.getPrice());
            preparedStatement.setString(4, productInsertView.getQuantity());
            preparedStatement.setString(5, productInsertView.getDesc());
            preparedStatement.setString(6, productInsertView.getBrand());
            preparedStatement.setLong(7, x);

            preparedStatement.executeUpdate();
            connection.commit();
        }
        catch (SQLException e) {
            try {
                connection.rollback();
            } catch (Exception ex) {
                throw new DataAccessException("Inserting person failed.");
            }
            throw new DataAccessException("Inserting person failed.");
           // logger.error(String.format("Something went wrong"));
        }
    }
}
