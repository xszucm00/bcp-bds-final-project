package org.but.feec.eshop.data;

import org.but.feec.eshop.App;
import org.but.feec.eshop.config.DataSourceConfig;
import org.but.feec.eshop.exceptions.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class SQLInjectionRepository {
    private  static final Logger logger = LoggerFactory.getLogger(SQLInjectionRepository.class);
    public void submitReview(String review) {
        try (Connection connection = DataSourceConfig.getConnection();
             Statement stmt = connection.createStatement()) {
            String query = String.format("INSERT INTO eshop.dummy (text)" +
                    " VALUES('%s');",review);
            stmt.execute(query);
        }
        catch (SQLException e) {
            throw new DataAccessException("Creating sql injection failed operation on the database failed.");
        }
    }
}
