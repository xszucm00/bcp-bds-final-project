package org.but.feec.eshop.controller;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.but.feec.eshop.api.ProductEditView;
import org.but.feec.eshop.api.ProductView;
import org.but.feec.eshop.data.ProductRepository;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class EditProductController {

    private static final Logger logger = LoggerFactory.getLogger(EditProductController.class);

    @FXML
    private TextField brandTextField;
    @FXML
    private TextField categoryTextField;
    @FXML
    private TextField descTextField;
    @FXML
    private Button editProductButton;
    @FXML
    private TextField idTextField;
    @FXML
    private TextField priceTextField;
    @FXML
    private TextField quantityTextField;
    @FXML
    private TextField sizeTextField;
    @FXML
    private TextField titleTextField;

    private ProductRepository productRepository;
    private ValidationSupport validation;

    // used to reference the stage and to get passed data through it
    public Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize() {
        productRepository = new ProductRepository();

        validation = new ValidationSupport();
        validation.registerValidator(idTextField, Validator.createEmptyValidator("The id must not be empty."));
        idTextField.setEditable(false);
        validation.registerValidator(categoryTextField, Validator.createEmptyValidator("The category must not be empty."));
        validation.registerValidator(brandTextField, Validator.createEmptyValidator("The brand must not be empty."));
        validation.registerValidator(titleTextField, Validator.createEmptyValidator("The title name must not be empty."));
        validation.registerValidator(sizeTextField, Validator.createEmptyValidator("The size must not be empty."));
        validation.registerValidator(descTextField, Validator.createEmptyValidator("The desc must not be empty."));
        validation.registerValidator(quantityTextField, Validator.createEmptyValidator("The quantity must not be empty."));
        validation.registerValidator(priceTextField, Validator.createEmptyValidator("The price must not be empty."));


        editProductButton.disableProperty().bind(validation.invalidProperty());

        loadProductData();

        logger.info("Product edit controller initialized");
    }

    private void loadProductData() {
        Stage stage = this.stage;
        if (stage.getUserData() instanceof ProductView) {
            ProductView productView = (ProductView) stage.getUserData();
            idTextField.setText(String.valueOf(productView.getId()));
            categoryTextField.setText(productView.getCategory());
            brandTextField.setText(productView.getBrand());
            titleTextField.setText(productView.getTitle());
            sizeTextField.setText(productView.getSize());
            descTextField.setText(productView.getDesc());
            quantityTextField.setText(productView.getQuantity());
            priceTextField.setText(String.valueOf(productView.getPrice()));


        }
    }
    private void personEditedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Person Edited Confirmation");
        alert.setHeaderText("Your person was successfully edited.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }

    @FXML
    public void handleEditProductButton(ActionEvent actionEvent) {
        Long id = Long.valueOf(idTextField.getText());
        String category = categoryTextField.getText();
        String brand = brandTextField.getText();
        String title = titleTextField.getText();
        String size = sizeTextField.getText();
        String desc = descTextField.getText();
        String quantity = quantityTextField.getText();
        Double price = Double.valueOf(priceTextField.getText());

        ProductEditView productEditView = new ProductEditView();
        productEditView.setId(id);
        productEditView.setBrand(brand);
        productEditView.setCategory(category);
        productEditView.setDesc(desc);
        productEditView.setPrice(price);
        productEditView.setQuantity(quantity);
        productEditView.setSize(size);
        productEditView.setTitle(title);

        productRepository.editProduct(productEditView);

        personEditedConfirmationDialog();
    }
}
