package org.but.feec.eshop.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.but.feec.eshop.api.ProductDetailedView;
import org.but.feec.eshop.api.ProductView;
import org.but.feec.eshop.data.ProductRepository;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DetailedProductViewController {
    private static final Logger logger = LoggerFactory.getLogger(DetailedProductViewController.class);
    @FXML
    private TextField brandTextField;
    @FXML
    private TextField categoryTextField;
    @FXML
    private TextField descTextField;
    @FXML
    private TextField idTextField;
    @FXML
    private TextField idUserTextField;
    @FXML
    private TextField priceTextField;
    @FXML
    private TextField quantityTextField;
    @FXML
    private TextField ratingTextField;
    @FXML
    private TextField reviewTextField;
    @FXML
    private TextField sizeTextField;
    @FXML
    private TextField titleTextField;
    public Stage stage;
    @FXML
    public void initialize() {

        loadProductData();

        logger.info("Product detailed view controller initialized");
    }
    private void loadProductData() {
        Stage stage = this.stage;
        if (stage.getUserData() instanceof ProductDetailedView) {
            ProductDetailedView productDetailedView = (ProductDetailedView) stage.getUserData();
            idTextField.setText(String.valueOf(productDetailedView.getId()));
            categoryTextField.setText(productDetailedView.getCategory());
            brandTextField.setText(productDetailedView.getBrand());
            titleTextField.setText(productDetailedView.getTitle());
            sizeTextField.setText(productDetailedView.getSize());
            descTextField.setText(productDetailedView.getDesc());
            quantityTextField.setText(productDetailedView.getQuantity());
            priceTextField.setText(String.valueOf(productDetailedView.getPrice()));
            reviewTextField.setText(productDetailedView.getReview());
            ratingTextField.setText(productDetailedView.getRating());
            idUserTextField.setText(String.valueOf(productDetailedView.getIdUser()));
        }
    }
    public void setStage(Stage stage) {
        this.stage = stage;
    }


}
