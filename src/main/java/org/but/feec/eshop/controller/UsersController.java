package org.but.feec.eshop.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import org.but.feec.eshop.App;
import org.but.feec.eshop.api.UsersView;
import org.but.feec.eshop.data.PersonRepository;
import org.but.feec.eshop.exceptions.ExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class UsersController {

    private static final Logger logger = LoggerFactory.getLogger(UsersController.class);

    @FXML
    private Button productTableButton;
    @FXML
    private TableColumn<UsersView,Integer > user_age;
    @FXML
    private TableColumn<UsersView, String> user_email;
    @FXML
    private TableColumn<UsersView, String> user_fname;
    @FXML
    private TableColumn<UsersView, Long> user_id;
    @FXML
    private TableColumn<UsersView, String> user_lname;
    @FXML
    private TableView<UsersView> usersTable;
    @FXML
    private Font x1;
    @FXML
    private Color x2;
    @FXML
    void handleProductTableButton(ActionEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("fxml/main.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 1050, 600);
            Stage stage = new Stage();
            stage.setTitle("Eshop database");
            stage.setScene(scene);

            Stage stageOld = (Stage) productTableButton.getScene().getWindow();
            stageOld.close();


            stage.show();
        } catch (IOException ex) {
            ExceptionHandler.handleException(ex);
        }
    }
    PersonRepository personRepository;
    @FXML
    private void initialize() {
        personRepository = new PersonRepository();

        user_id.setCellValueFactory(new PropertyValueFactory<UsersView, Long>("id"));
        user_fname.setCellValueFactory(new PropertyValueFactory<UsersView, String>("firstName"));
        user_lname.setCellValueFactory(new PropertyValueFactory<UsersView, String>("lastName"));
        user_age.setCellValueFactory(new PropertyValueFactory<UsersView, Integer>("age"));
        user_email.setCellValueFactory(new PropertyValueFactory<UsersView, String>("email"));


        ObservableList<UsersView> observablePersonsList = initializePersonsData();
        usersTable.setItems(observablePersonsList);

        usersTable.getSortOrder().add(user_id);


        logger.info("PersonsController initialized");
    }
    private ObservableList<UsersView> initializePersonsData() {
        List<UsersView> users = personRepository.getUsersBasicView();
        return FXCollections.observableArrayList(users);
    }

}
