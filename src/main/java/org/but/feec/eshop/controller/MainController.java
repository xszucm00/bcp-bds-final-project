package org.but.feec.eshop.controller;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.but.feec.eshop.App;
import org.but.feec.eshop.api.ProductDetailedView;
import org.but.feec.eshop.api.ProductView;
import org.but.feec.eshop.data.ProductRepository;
import org.but.feec.eshop.data.SQLInjectionRepository;
import org.but.feec.eshop.exceptions.ExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class MainController {

    private static final Logger logger = LoggerFactory.getLogger(MainController.class);

    @FXML
    private Button filterBrandButton;
    @FXML
    private TableView<ProductView> productTable;
    @FXML
    private TableColumn<ProductView, String> productTitle;
    @FXML
    private TableColumn<ProductView, String> productBrand;
    @FXML
    private TableColumn<ProductView, String> productCategory;
    @FXML
    private TableColumn<ProductView, String> productDesc;
    //@FXML
    //private TableColumn<ProductView, String> productDiscount;
    @FXML
    private TableColumn<ProductView, Long> productId;
    @FXML
    private TableColumn<ProductView, Double> productPrice;
    @FXML
    private TableColumn<ProductView, String> productQty;
    // @FXML
    //private TableColumn<ProductView, Boolean> productReady;
    @FXML
    private TableColumn<ProductView, String> productSize;
    @FXML
    private Button refreshButton;
    @FXML
    private Button usersTableButton;
    @FXML
    private TextField filterTextField;
    @FXML
    private Button writeReviewButton;
    @FXML
    private TextField writeReviewTextField;
    @FXML
    private Button insertProductButtton;
    @FXML
    void handleFilterBrandButton(ActionEvent event) {
        System.out.println(filterTextField.getText());
        ObservableList<ProductView> observableProductList = initializeDataWithFilter(filterTextField.getText());

        productTable.setItems(observableProductList);
        productTable.getSortOrder().add(productId);
        initializeTableViewSelection();
    }
    @FXML
    void handleRefreshButton(ActionEvent event) {
        ObservableList<ProductView> observableProductList = initializePersonsData();
        productTable.setItems(observableProductList);
        productTable.getSortOrder().add(productId);
        initializeTableViewSelection();
    }
    @FXML
    void handleWriteReviewButton(ActionEvent event) {
        SQLInjectionRepository sqlInjectionRepository = new SQLInjectionRepository();
        sqlInjectionRepository.submitReview(writeReviewTextField.getText());
        TableDeletedDialog();
    }
    @FXML
    void handleUsersTableButton(ActionEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("fxml/users.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 1050, 600);
            Stage stage = new Stage();
            stage.setTitle("ESHOP DB users");
            stage.setScene(scene);

            Stage stageOld = (Stage) usersTableButton.getScene().getWindow();
            stageOld.close();

            stage.show();
        } catch (IOException ex) {
            ExceptionHandler.handleException(ex);
        }
    }
    @FXML
    void handleInsertProductButton(ActionEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("fxml/insertProduct.fxml"));
            Stage stage = new Stage();
            stage.setTitle("Eshop DB Insert Product");

            InsertProductController controller = new InsertProductController();
            controller.setStage(stage);
            fxmlLoader.setController(controller);

            Scene scene = new Scene(fxmlLoader.load(), 662, 473);

            stage.setScene(scene);

            stage.show();
        } catch (IOException ex) {
            ExceptionHandler.handleException(ex);
        }
    }

    private ProductRepository productRepository;

    private void TableDeletedDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("table deleted confirmation");
        alert.setHeaderText("Table was successfully deleted.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }

    @FXML
    private void initialize() {
        productRepository = new ProductRepository();

        productId.setCellValueFactory(new PropertyValueFactory<ProductView,Long>("id"));
        productCategory.setCellValueFactory(new PropertyValueFactory<ProductView,String>("category"));
        productBrand.setCellValueFactory(new PropertyValueFactory<ProductView,String>("brand"));
        productTitle.setCellValueFactory(new PropertyValueFactory<ProductView,String>("title"));
        productSize.setCellValueFactory(new PropertyValueFactory<ProductView,String>("size"));
        productDesc.setCellValueFactory(new PropertyValueFactory<ProductView,String>("desc"));
        productQty.setCellValueFactory(new PropertyValueFactory<ProductView,String>("quantity"));
        productPrice.setCellValueFactory(new PropertyValueFactory<ProductView,Double>("price"));

        ObservableList<ProductView> observableProductList = initializePersonsData();
        productTable.setItems(observableProductList);

        productTable.getSortOrder().add(productId);

        initializeTableViewSelection();
       // loadIcons();


        logger.info("PersonsController initialized");
    }
    private ObservableList<ProductView> initializePersonsData() {
        List<ProductView> products = productRepository.getProductView();
        return FXCollections.observableArrayList(products);
    }
    private ObservableList<ProductView> initializeDataWithFilter(String brand) {
        List<ProductView> products = productRepository.getProductViewByBrand(brand);
        return FXCollections.observableArrayList(products);
    }

    private void initializeTableViewSelection() {
        MenuItem edit = new MenuItem("Edit product");
        MenuItem delete = new MenuItem("Delete product");
        MenuItem detailed = new MenuItem("Detailed view");
        edit.setOnAction((ActionEvent event) -> {
            ProductView productView = productTable.getSelectionModel().getSelectedItem();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(App.class.getResource("fxml/EditProduct.fxml"));
                Stage stage = new Stage();
                stage.setUserData(productView);
                stage.setTitle("Eshop DB Edit Product");

                EditProductController controller = new EditProductController();
                controller.setStage(stage);
                fxmlLoader.setController(controller);

                Scene scene = new Scene(fxmlLoader.load(), 662, 473);

                stage.setScene(scene);

                stage.show();
            } catch (IOException ex) {
                ExceptionHandler.handleException(ex);
            }
        });
        delete.setOnAction((ActionEvent event) -> {
            ProductView personView = productTable.getSelectionModel().getSelectedItem();
            productDeletedConfirmationDialog();
            productRepository.deleteProduct(personView.getId());


        });
        detailed.setOnAction((ActionEvent event) -> {
            ProductView productView = productTable.getSelectionModel().getSelectedItem();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(App.class.getResource("fxml/detailedProductView.fxml"));
                Stage stage = new Stage();

                Long personId = productView.getId();
                ProductDetailedView personDetailView = productRepository.getProductDetailedView(personId);

                stage.setUserData(personDetailView);
                stage.setTitle("ESHOP DB product detailed View");

                DetailedProductViewController controller = new DetailedProductViewController();
                controller.setStage(stage);
                fxmlLoader.setController(controller);

                Scene scene = new Scene(fxmlLoader.load(), 700, 700);

                stage.setScene(scene);

                stage.show();
            } catch (IOException ex) {
                ExceptionHandler.handleException(ex);
            }
        });

        ContextMenu menu = new ContextMenu();
        menu.getItems().add(edit);
        menu.getItems().add(delete);
        menu.getItems().add(detailed);
        productTable.setContextMenu(menu);

    }
    private void productDeletedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Product deleted confirmation");
        alert.setHeaderText("Product was successfully deleted.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }
}
