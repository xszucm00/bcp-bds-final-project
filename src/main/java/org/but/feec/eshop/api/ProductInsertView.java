package org.but.feec.eshop.api;

public class ProductInsertView {

    private String category;
    private String brand;
    private String title;
    private String size;
    private String desc;
    private String quantity;
    private Double price;


    public String getCategory(){return this.category;}
    public void setCategory(String category){this.category=category;}
    public String getBrand(){return this.brand;}
    public void setBrand(String brand){this.brand=brand;}
    public String getTitle(){return this.title;}
    public void setTitle(String title){this.title=title;}
    public String getSize(){return this.size;}
    public void setSize(String size){this.size=size;}
    public String getDesc(){return this.desc;}
    public void setDesc(String desc){this.desc=desc;}
    public String getQuantity(){return this.quantity;}
    public void setQuantity(String quantity){this.quantity=quantity;}
    public Double getPrice(){return this.price;}
    public void setPrice(Double price){this.price=price;}

}
