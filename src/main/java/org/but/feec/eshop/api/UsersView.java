package org.but.feec.eshop.api;

import javafx.beans.property.*;

public class UsersView {
    private LongProperty id = new SimpleLongProperty();
    private StringProperty first_name = new SimpleStringProperty();
    private StringProperty last_name = new SimpleStringProperty();
    private StringProperty email = new SimpleStringProperty();
    private IntegerProperty age = new SimpleIntegerProperty();

    public Long getId() {
        return idProperty().get();
    }
    public void setId(Long id) {
        this.idProperty().setValue(id);
    }
    public String getFirstName() {
        return first_nameProperty().get();
    }
    public void setFirstName(String firstName) {
        this.first_nameProperty().setValue(firstName);
    }
    public String getEmail() {
        return emailProperty().get();
    }
    public void setEmail(String email) {
        this.emailProperty().setValue(email);
    }
    public String getLastName() {
        return last_nameProperty().get();
    }
    public void setLastName(String last_name) {
        this.last_nameProperty().setValue(last_name);
    }
    public Integer getAge() {
        return ageProperty().get();
    }
    public void setAge(Integer age) {
        this.ageProperty().setValue(age);
    }


    public LongProperty idProperty() {
        return id;
    }
    public StringProperty first_nameProperty() {
        return first_name;
    }
    public StringProperty emailProperty() {
        return email;
    }
    public StringProperty last_nameProperty() {
        return last_name;
    }
    public IntegerProperty ageProperty() {
        return age;
    }

}
