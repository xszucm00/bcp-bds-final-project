package org.but.feec.eshop.service;

import org.but.feec.eshop.data.PersonRepository;
import at.favre.lib.crypto.bcrypt.BCrypt;
import org.but.feec.eshop.exceptions.ResourceNotFoundException;
import org.but.feec.eshop.api.UserAuthView;


public class AuthService {
    private PersonRepository personRepository;

    public AuthService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    private UserAuthView findPersonByEmail(String email) {
        return personRepository.findUserByEmail(email);
    }

    public boolean authenticate(String email, String password) {
        if (email == null || email.isEmpty() || password == null || password.isEmpty()) {
            return false;
        }

        UserAuthView userAuthView = findPersonByEmail(email);
        if (userAuthView == null) {
            throw new ResourceNotFoundException("Provided username is not found.");
        }
    /*   if (userAuthView.getPassword().equals(password)){
            System.out.println(userAuthView.getPassword() + password + "TRUE");
            System.out.println();
            return true;
        }
        else{
            System.out.println(userAuthView.getPassword() + password + "    false");
            System.out.println(password);
            return false;}
    */
      //  String passwordlmao="$2a$12$xtg2pjNnzXcepbOaK2lrJOmku2ithQMeGzWBFcGVUI754rtQ3xtmC";
      //  BCrypt.Result result = BCrypt.verifyer().verify(password.toCharArray(), passwordlmao);
        BCrypt.Result result = BCrypt.verifyer().verify(password.toCharArray(), userAuthView.getPassword());
        return result.verified;
    }

}
