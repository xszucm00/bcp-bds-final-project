**E-Commerce database**

**Description:**
Database with features like login, filter through database, remove, add, edit.

**Needed for the application:**

1. Maven
2. Postgres

**Instructions to turn on the application:**
1. upload database file 'eshop_db' to your postgres databases
2. move to the project directory
3. in cmd:
	1.     mvn clean install
	2.     java -jar target\bds-eshop-1.0.0.jar
4. login with:
- login: hportall@mac.com	
- password: batman

_Author: Martin Szüč, VUT FEKT_
